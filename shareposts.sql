-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2021 at 02:19 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shareposts`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `created_at`) VALUES
(1, 2, 'Измий чиниите ', 'Lorem ipsum е един от най-често използваните в печатарството и графичния дизайн заготовъчни текстове, служещи да запълват с текст онези графични елементи на документ или графична презентация, които трябва да бъдат представени със собствен шрифт и типография, запълвайки ги със стандартен, неотвличащ вниманието', '2021-07-29 09:02:36'),
(2, 2, 'Намери проекти и измисли, нещо за инвестиране', 'Lorem ipsum е един от най-често използваните в печатарството и графичния дизайн заготовъчни текстове, служещи да запълват с текст онези графични елементи на документ или графична презентация, които трябва да бъдат представени със собствен шрифт и типография, запълвайки ги със стандартен, неотвличащ вниманието', '2021-07-29 09:02:36'),
(5, 2, 'asd 2 333', 'asd', '2021-07-29 10:24:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`) VALUES
(2, 'Ivan Balkanov', 'ibalkanov@gmail.com', '$2y$10$B69ubq324P.aBmRvBRy5ku60ht.aCG8rXd/lQzqlTRD1KXnFdivK2', '2021-07-28 16:22:47'),
(3, 'asd', 'asd@asd.com', '$2y$10$CjS8c7ikBWqYgveKwQ2UuuJx.L7MBhaOKRtc8x1sJubGhNdq0JBtC', '2021-07-29 07:53:26'),
(4, 'Ivan Balkanov', 'asd2@asd.com', '$2y$10$Q.jJVObPLZ8v3leKrQjTueXasHImju.8FOPdc6vcpulnFibwuxXN.', '2021-07-29 08:16:21'),
(5, 'Ivan Balkanov', 'asd3@asd.com', '$2y$10$qIi4g6ttO64l0ax5J4lA6.V9lRGmEaMlMqDv6bZ.p/pHg.UZmYnJi', '2021-07-29 08:18:14'),
(6, 'zara', 'zara@zara.com', '$2y$10$O5Yx5YvZA0hMKoCDxjc0.emDMrU.Ec3fX65EWVVTfgV5mmzpKXldi', '2021-07-29 10:23:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
