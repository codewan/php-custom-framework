<?php
// DB params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'shareposts');


// App Root
define( 'APPROOT', dirname( dirname(__FILE__)));

// URL Root
define('URLROOT', 'http://localhost/php-custom-framework');

// Site Name
define('SITENAME', 'php-custom-framework');

// App Version
define('APPVERSION', '1.0.0');